#!/bin/python

# link to the examples - https://diagrams.mingrammer.com/docs/getting-started/examples

from diagrams import Cluster, Diagram, Edge

from diagrams.onprem.proxmox import Pve
from diagrams.onprem.compute import Server
from diagrams.generic.compute import Rack
from diagrams.generic.network import Firewall,Router,Subnet,Switch,VPN
from diagrams.generic.blank import Blank
from diagrams.onprem.container import Lxc,Docker,Containerd
from diagrams.onprem.database import Influxdb,Mariadb,Postgresql,Mysql
from diagrams.onprem.network import Internet
from diagrams.generic.device import Mobile,Tablet
from diagrams.onprem.client import Client
from diagrams.generic.os import Android,LinuxGeneral,Windows
from diagrams.onprem.proxmox import Pve

# connection leggend
# black = wired connetion
# red = wireless connection
# blue = virtual connection
# green = SO


with Diagram(name="HomeLab-2020", outformat="svg", show=False):
    # generic stuff
    wan = Internet("WAN")
    android = Android("LineageOS")
    linux_A = LinuxGeneral("Arch")
    linux_D = LinuxGeneral("Debian")
    linux_N = LinuxGeneral("NixOS")
    linux_U = LinuxGeneral("Ubuntu")

    # infrastructure devices
    router = Router("Fritzbox 3490")
    repeater = Router("Fritxbox repeater 310")
    switch = Switch("Dlink-unmanaged")

    # iot stuff
    with Cluster("IOT shit"):
        grpIOT = [
                Client("Shelly T/H #1"),
                Client("Shelly T/H #2"),
                Client("Shelly T/H #3"),
                Client("Tplink light #1"),
                Client("Tplink light #2"),
                Client("Neato D6")
      ]

    # consume devices
    with Cluster("Consume devices"):
        grpCND = [
                Client("Printer Samsung m2835dw"),
                Client("Samsung tv-j6400uk")
       ]

    # personal devices
    with Cluster("Personal devices"):
        grpPRS = [
                linux_U - linux_N - linux_A - Edge(color="green", style="bold") - Client("Dell g5 5587"),
                android - Edge(color="green", style="bold") - Client("Pixel 5")
       ]

    # virtual services [LXC/Docker]
    with Cluster("virtual services"):
        grpSVC = [
                linux_D - Edge(color="green", style="bold") - Lxc("Nextcloud"),
                linux_D - Edge(color="green", style="bold") - Lxc("MLdonkey"),
                linux_D - Edge(color="green", style="bold") - Lxc("LogAnalyzer"),
                linux_D - Edge(color="green", style="bold") - Lxc("Plex"),
                linux_D - Edge(color="green", style="bold") - Lxc("NFS"),
                linux_D - Edge(color="green", style="bold") - Lxc("RainLoop"),
                linux_D - Edge(color="green", style="bold") - Lxc("Firefly-iii"),
                linux_D - Edge(color="green", style="bold") - Lxc("Grafana/Influxdb"),
                linux_D - Edge(color="green", style="bold") - Lxc("tt-rss"),
                linux_D - Edge(color="green", style="bold") - Lxc("HomeAssistant"),
                linux_D - Edge(color="green", style="bold") - Lxc("PiAlert"),
                linux_D - Edge(color="green", style="bold") - Lxc("PiHole"),
                linux_D - Edge(color="green", style="bold") - Lxc("VaultWarden"),
                linux_D - Edge(color="green", style="bold") - Lxc("PaperMerge")
       ]

    # hp microserver gen 10
    with Cluster("pve Node #1"):
            main = Pve("Node #1") - Edge(color="green", style="bold") - Server("HP userver gen10")
            main - Edge(color="blue", style="bold") - grpSVC
            switch >> Edge(color="black", style="bold") >>main


    # connection
    wan >> Edge(color="black", style="bold") >> router >> Edge(color="black", style="bold") >>switch

    switch >> Edge(color="black", style="bold") >> grpCND

    router >> Edge(color="red", style="bold") >> repeater >> Edge(color="red", style="bold") >>grpIOT

    router >> Edge(color="red", style="bold") >> grpPRS


